<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $article = Article::all();
        return response() -> json([
            'status' => '200',
            'message' => 'Article list',
            'article' => $article,
        ]);
    }

    public function show($id)
    {
        $article = Article::find($id);
        if($article){
            return response()->json([
                'status code'=>200,
                'message'=>'show article success',
                'article'=>$article]
                );
            }
            return response()->json([
                'status code'=>404,
               'message'=>'Article id '.$id.' not found']
                ,404);
    }

    public function store(StorePostRequest $request)
    {

           
            

        $article = Article::create($request->all());
        return response() -> json([
            'status' => '201',
            'message' => 'Create Success',
            'article' => $article,
        ]);


    }

    public function update(StorePostRequest $request, $id)
    {

        $article =Article::find($id);
        if($article){
       
        $article->update($request->all());
        return response()->json([
            'status code'=>200,
            'message'=>'edit article success',
            'article'=>$article]
            );
        } return response()->json([
            'status code'=>404,
            'message'=>'Not found']
            );
    }

    public function destroy($id)
    {
        $article=Article::find($id);
       if($article){
        $article->delete();
        return response()->json([
            'status code'=>200,
            'message'=>'delete article success']
             ,200);
       }
       return response()->json([
            'status code'=>404,
            'message'=>'Not found']
            ,404);

    }

}


