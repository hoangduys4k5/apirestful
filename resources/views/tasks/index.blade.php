@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <form action=""  class="form-inline" role="form">
            <div class="form-group">
                <input class="form-control" type="" name="key" placeholder="Search by name....">
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Search</button>
        </form>
        <hr>
        <table class="table table-striped">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Content</th>
                <th>Action</th>
            </tr>
            @foreach($tasks as $task)
            <tr>
                <th>{{ $task->id }}</th>
                <th>{{ $task->name }}</th>
               <th>{{ $task->content }}</th>
               <th>
                   <form method="post" action="{{route('tasks.destroy',$task->id)}}">
                    @csrf   
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Delete this tasks?')" ; class=" btn btn-danger">Delete</button>
                    <a href="{{route('tasks.edit',$task->id)}}" class="btn btn-warning">Edit</a>
                    <a class="btn btn-info" href="{{ route('tasks.show',$task->id) }}">Show</a>
                   </form>
               </th>
            </tr>
            @endforeach
        </table>
        {{ $tasks->appends(request()->all())->links() }}
</div>


@endsection